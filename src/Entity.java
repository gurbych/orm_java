package junior.databases.homework;

import java.util.*;
import java.util.Date;
import java.sql.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.google.common.base.Joiner;

public abstract class Entity {
    private static String DELETE_QUERY   = "DELETE FROM \"%1$s\" WHERE %1$s_id=?";
    private static String INSERT_QUERY   = "INSERT INTO \"%1$s\" (%2$s) VALUES (%3$s) RETURNING %1$s_id";
    private static String LIST_QUERY     = "SELECT * FROM \"%s\"";
    private static String SELECT_QUERY   = "SELECT * FROM \"%1$s\" WHERE %1$s_id=?";
    private static String CHILDREN_QUERY = "SELECT * FROM \"%1$s\" WHERE %2$s_id=?";
    private static String SIBLINGS_QUERY = "SELECT * FROM \"%1$s\" NATURAL JOIN \"%2$s\" WHERE %3$s_id=?";
    private static String UPDATE_QUERY   = "UPDATE \"%1$s\" SET %2$s WHERE %1$s_id=?";

    private static Connection db = null;

    protected boolean isLoaded = false;
    protected boolean isModified = false;
    private String table = null;
    private int id = 0;
    protected Map<String, Object> fields = new HashMap<String, Object>();


    public Entity() {
        this(0);
    }

    public Entity(Integer id) {
        this.id = id;
        table = this.getClass().getSimpleName().toLowerCase();
    }

    public static final void setDatabase(Connection connection) {	
		if ( connection == null ) {
    		throw new NullPointerException("No connection!");
    	}
		db = connection;
    }

    public final int getId() {
        return id;
    }

    public final java.util.Date getCreated() {
        // try to guess youtself
    	return getDate(String.format("%s_created", table));
    }
    
    public final java.util.Date getUpdated() {
        // try to guess youtself
    	return getDate(String.format("%s_updated", table));
    }

    public final Object getColumn(String name) {
        // return column name from fields by key
    	load();
        return fields.get(String.format("%s_id", name));
    }

    public final <T extends Entity> T getParent(Class<T> cls) {
        // get parent id from fields as <classname>_id, create and return an instance of class T with that id
    	load();
    	
    	String parentColumnName = cls.getClass().getName().toLowerCase() + "_id";
    	int parentId = (int) this.fields.get(parentColumnName);
    
    	try {
			return cls.getDeclaredConstructor(Integer.class).newInstance(parentId);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    }

    public final <T extends Entity> List<T> getChildren(Class<T> cls) {
        // select needed rows and ALL columns from corresponding table
        // convert each row from ResultSet to instance of class T with appropriate id
        // fill each of new instances with column data
        // return list of children instances

    	String query = String.format(CHILDREN_QUERY, cls.getSimpleName().toLowerCase(), this.table);

    	try {
    		PreparedStatement ps = db.prepareStatement(query);
    		
			ps.setInt(1, id);
			
			ResultSet rs = ps.executeQuery();
			
			return rowsToEntities(cls, rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
        return null;
    }

    public final <T extends Entity> List<T> getSiblings(Class<T> cls) {
        // select needed rows and ALL columns from corresponding table
        // convert each row from ResultSet to instance of class T with appropriate id
        // fill each of new instances with column data
        // return list of sibling instances
    	String targetTableName = cls.getSimpleName().toLowerCase();
    	String joinTableName = getJoinTableName(targetTableName, this.table);
    	String query = String.format(SIBLINGS_QUERY, targetTableName, joinTableName, this.table);
    	
    	try{
    		PreparedStatement ps = db.prepareStatement(query);
    		
    		ps.setInt(1, this.id);
    		
    		ResultSet rs = ps.executeQuery();
    		
    		return rowsToEntities(cls, rs);
    	} catch (SQLException e) {
    		e.printStackTrace();
    	}
        return null;
    }

    public final void setColumn(String name, Object value) {
        // put a value into fields with <table>_<name> as a key
    	load();
    	String key = String.format("%s_%s", table, name);
    	
    	fields.put(key, value);
    	isModified = true;
    }

    public final void setParent(String name, Integer id) {
        // put parent id into fields with <name>_<id> as a key
    	fields.put(String.format("%s_id", name), id);
    	isModified = true;
    }

    private void load() {
        // check, if current object is already loaded
        // get a single row from corresponding table by id
        // store columns as object fields with unchanged column names as keys
    	if ( isLoaded || db == null ) {
    		return;
    	}
    	
    	try {
			PreparedStatement ps = db.prepareStatement(String.format(SELECT_QUERY, table));
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
		
			while (rs.next()) {
				for (int i = 1; i <= columnsNumber; i++) {
					this.fields.put(rsmd.getColumnName(i), rs.getObject(i));
				}
			}
			isLoaded = true;
    	} catch (SQLException e) {
    		e.printStackTrace();
    	}
    }

    private void insert() throws SQLException {
        // execute an insert query, built from fields keys and values
    	String columnNames = join(this.fields.keySet());
    	String values = Joiner.on(", ").join(fields.values());
    	String insertQuery = String.format(INSERT_QUERY, this.table, columnNames, values);
    	PreparedStatement insert = db.prepareStatement(insertQuery);
    	
    	System.out.println("Rows inserted: " + insert.executeUpdate());
    }

    private void update() throws SQLException {
        // execute an update query, built from fields keys and values
        String columnNames = join(this.fields.keySet(), "=?, ") + "=?, ";
        String updateQuery = String.format(UPDATE_QUERY, this.table, columnNames);
        PreparedStatement update = db.prepareStatement(updateQuery);
        Object[] values = this.fields.values().toArray();
        int size = this.fields.size();

        for ( int i = 1; i <= size; i++ ) {
            update.setObject(i, values[i-1]);
        }
        update.setInt(size+1, this.id);
        
        System.out.println("Rows updated: " + update.executeUpdate());
    }

    public final void delete() throws SQLException {
        // execute a delete query with current instance id
        String deleteQuery = String.format(DELETE_QUERY, this.table);
        PreparedStatement delete = db.prepareStatement(deleteQuery);

        delete.setInt(1, this.id);
        System.out.println("Rows deleted: " + delete.executeUpdate());
    }

    public final void save() throws SQLException {
        // execute either insert or update query, depending on instance id
    	if ( !isModified ) {
    		this.insert();
    	} else {
    		this.update();
    	}
    	this.isModified = false;
    }

    protected static <T extends Entity> List<T> all(Class<T> cls) {
        // select ALL rows and ALL columns from corresponding table
        // convert each row from ResultSet to instance of class T with appropriate id
        // fill each of new instances with column data
        // aggregate all new instances into a single List<T> and return it
    	
        String tableName = cls.getSimpleName().toLowerCase();
        String query = String.format(LIST_QUERY, tableName);
        try {
        	Statement statement = db.createStatement();
        	ResultSet rs = statement.executeQuery(query);
        	return rowsToEntities(cls, rs);
        } catch (SQLException e) {
        	e.printStackTrace();
        }
        return null;
    }

    private static Collection<String> genPlaceholders(int size) {
        // return a string, consisting of <size> "?" symbols, joined with ", "
        // each "?" is used in insert statements as a placeholder for values (google prepared statements)
        return genPlaceholders(size, "?");
    }

    private static Collection<String> genPlaceholders(int size, String placeholder) {
        // return a string, consisting of <size> <placeholder> symbols, joined with ", "
        // each <placeholder> is used in insert statements as a placeholder for values (google prepared statements)
        ArrayList<String> placeholders = new ArrayList<String>();
        
        for ( int i = 0; i < size; i++ ) {
        	placeholders.add(i, placeholder);
        }
    	return placeholders;
    }

    private static String getJoinTableName(String leftTable, String rightTable) {
        // generate the name of associative table for many-to-many relation
        // sort left and right tables alphabetically
        // return table name using format <table>__<table>
    	
    	ArrayList<String> joinTableNames = new ArrayList<String>();
    	
    	joinTableNames.add(0, leftTable);
    	joinTableNames.add(1, rightTable);
    	
    	Collections.sort(joinTableNames);
    	
        return join(joinTableNames, "__");
    }

    private java.util.Date getDate(String column) {
        // pwoerful method, used to remove copypaste from getCreated and getUpdated methods
    	load();
        return (Date) fields.get(column);
    }
    
    private static String join(Collection<String> sequence) {
        // join collection of strings with ", " as glue and return a joined string
    	return join(sequence, ", ");
    }

    private static String join(Collection<String> sequence, String glue) {
        // join collection of strings with glue and return a joined string
    	if ( sequence.isEmpty() ) {
    		return "";
    	}
    	
    	Iterator<String> iter = sequence.iterator();
    	StringBuilder sb = new StringBuilder();
    	
    	if (iter.hasNext()) {
    	  sb.append(iter.next());
    	  while (iter.hasNext()) {
    	    sb.append(glue).append(iter.next());
    	  }
    	}
    	
        return sb.toString();
    }

    private static <T extends Entity> List<T> rowsToEntities(Class<T> cls, ResultSet rows) {
        // convert a ResultSet of database rows to list of instances of corresponding class
        // each instance must be filled with its data so that it must not produce additional queries to database to get it's fields
        ArrayList<T> list = new ArrayList<T>();
        
        try {
        	Constructor<T> constructor = cls.getConstructor(Integer.class);
        	Map<String, Object> fields = new HashMap<String, Object>();
        	String columnId = String.format("%s_id", cls.getSimpleName().toLowerCase());
        	ResultSetMetaData rsmd = rows.getMetaData();
        	int numberOfColumns = rsmd.getColumnCount();
        	int id;
        	T entity;

        	while ( rows.next() ) {
        		for ( int i = 1; i <= numberOfColumns; i++ ) {
        			fields.put(rsmd.getColumnName(i), rows.getObject(i));
        		}

        		id = (int) fields.get(columnId);
        		entity = constructor.newInstance(id);
        		entity.fields = fields;
        		entity.isLoaded = true;
        		list.add(entity);
        	}
        } catch (Exception e) {
        	e.printStackTrace();
        }
    	return list;
    }
}
